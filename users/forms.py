from django import forms
from .models import BeAuthor

class BeAuthorForm(forms.ModelForm):
    class Meta:
        model = BeAuthor
        exclude = ['is_view','user','canceled']



